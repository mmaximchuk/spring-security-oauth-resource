package com.maximchuk.oauth.resource;

import org.springframework.security.core.GrantedAuthority;

import java.util.List;

/**
 * Created by Maxim Maximchuk
 * date 04-Mar-16.
 */
public interface OAuthUser {

    List<? extends GrantedAuthority> getRoles();

}
