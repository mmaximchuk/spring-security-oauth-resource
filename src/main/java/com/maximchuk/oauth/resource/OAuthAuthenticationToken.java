package com.maximchuk.oauth.resource;

import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * Created by Maxim Maximchuk
 * date 04-Mar-16.
 */
public class OAuthAuthenticationToken extends AbstractAuthenticationToken {

    private OAuthPrincipal principles;

    public OAuthAuthenticationToken(OAuthPrincipal principles) {
        super(principles.getUser().getRoles());
        this.principles = principles;
        setAuthenticated(true);
    }

    @Override
    public String getCredentials() {
        return "";
    }

    @Override
    public OAuthPrincipal getPrincipal() {
        return principles;
    }
}
