package com.maximchuk.oauth.resource;

import com.maximchuk.rest.client.http.HttpException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.io.IOException;

/**
 * @author Maxim Maximchuk
 *         date 04.03.2016.
 */
public class OAuthResourceAuthenticationProvider implements AuthenticationProvider {

    private OAuthResourceAuthenticationService authService;
    private String authServerUrl;

    public OAuthResourceAuthenticationProvider(String authServerUrl, OAuthResourceAuthenticationService authService) {
        this.authService = authService;
        this.authServerUrl = authServerUrl;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            if (authentication.getCredentials() != null) {
                try {
                    AuthServerClient client = new AuthServerClient(authServerUrl);
                    JSONObject res = client.checkToken((String) authentication.getCredentials());
                    String username = res.getString("username");
                    String clientId = res.getString("client_id");

                    authentication.setAuthenticated(true);
                    return new OAuthAuthenticationToken(new OAuthPrincipal(authService.processUser(username),
                            authService.processClientId(clientId)));
                } catch (AuthenticationException e) {
                    throw new BadCredentialsException(e.getMessage(), e);
                } catch (HttpException e) {
                    switch (HttpStatus.valueOf(e.getErrorCode())) {
                        case UNAUTHORIZED:
                            throw new BadCredentialsException("Invalid bearer token");
                        case NOT_FOUND:
                            throw new AuthenticationServiceException("Auth server unavailable");
                        default:
                            throw e;
                    }
                }
            } else {
                throw new AuthenticationCredentialsNotFoundException("Authorization header with bearer token is not found");
            }
        } catch (HttpException | IOException e) {
            throw new AuthenticationServiceException(e.getMessage(), e);
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
