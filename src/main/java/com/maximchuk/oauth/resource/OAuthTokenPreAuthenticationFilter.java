package com.maximchuk.oauth.resource;

import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Maxim Maximchuk
 * date 04-Mar-16.
 */
public class OAuthTokenPreAuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter {

    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER_TOKEN_TYPE = "Bearer";

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        return "";
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        String header = request.getHeader(AUTHORIZATION);
        return header != null && header.contains(BEARER_TOKEN_TYPE)? header.replace(BEARER_TOKEN_TYPE, "").trim(): null;
    }
}
