package com.maximchuk.oauth.resource;

import org.springframework.security.core.AuthenticationException;

/**
 * @author Maxim Maximchuk
 *         date 04.03.2016.
 */
public interface OAuthResourceAuthenticationService {

    OAuthUser processUser(String username) throws AuthenticationException;

    String processClientId(String clientId) throws AuthenticationException;

}
