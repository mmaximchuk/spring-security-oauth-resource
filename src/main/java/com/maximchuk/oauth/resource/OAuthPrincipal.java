package com.maximchuk.oauth.resource;

/**
 * Created by Maxim Maximchuk
 * date 04-Mar-16.
 */
public class OAuthPrincipal {

    private OAuthUser user;
    private String clientId;

    public OAuthPrincipal(OAuthUser user, String clientId) {
        this.user = user;
        this.clientId = clientId;
    }

    @SuppressWarnings("unchecked")
    public <T extends OAuthUser> T getUser() {
        return (T)user;
    }

    public String getClientId() {
        return clientId;
    }
}
