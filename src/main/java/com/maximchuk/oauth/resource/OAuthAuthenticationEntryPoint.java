package com.maximchuk.oauth.resource;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Maxim Maximchuk
 * date 04-Mar-16.
 */
@Component
public class OAuthAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        AuthenticationException authException = (AuthenticationException)request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        if (authException instanceof AuthenticationServiceException) {
            response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, authException.getMessage());
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());
        }
    }

}
