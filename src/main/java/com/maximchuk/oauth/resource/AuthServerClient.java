package com.maximchuk.oauth.resource;

import com.maximchuk.rest.client.core.DefaultClient;
import com.maximchuk.rest.client.core.RestApiMethod;
import com.maximchuk.rest.client.http.HttpException;
import org.json.JSONObject;

import java.io.IOException;

/**
* @author Maxim Maximchuk
*         date 23.12.14.
*/
public class AuthServerClient extends DefaultClient {

    private static final String CONTROLLER_NAME = "api/session";

    public AuthServerClient(String serverUrl) {
        super(serverUrl, CONTROLLER_NAME);
    }

    public JSONObject checkToken(String token) throws IOException, HttpException {
        RestApiMethod method = new RestApiMethod("check", RestApiMethod.Type.GET);
        method.putParam("token", token);
        return new JSONObject(executeMethod(method).getString());
    }
}
